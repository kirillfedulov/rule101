#!/usr/bin/perl

use warnings;
use strict;

my $width = 32;
my $height = 30;
my $maxiter = 1;

my %table = (
	0 => 0,
	1 => 1,
	2 => 1,
	3 => 1,
	4 => 0,
	5 => 1,
	6 => 1,
	7 => 0
);

my @cells = ();

for (my $i = 0; $i < $width * $height; $i++) {
	$cells[$i] = 0;
}

$cells[$width-2] = 1;

for (my $iter = 0; $iter < $maxiter; $iter++) {
	for (my $i = 0; $i < $height-1; $i++) {
		for (my $j = 1; $j < $width-1; $j++) {
			my $idx = $width * $i + $j;
			my $pat = ($cells[$idx - 1] << 2) |
			          ($cells[$idx] << 1) |
				   $cells[$idx + 1];
			$cells[$width * ($i + 1) + $j] = $table{$pat};
		}
	}

	print "-" x $width;
	print "\n";

	for (my $i = 0; $i < $height; $i++) {
		for (my $j = 0; $j < $width; $j++) {
			if ($cells[$width * $i + $j] == 1) {
				print "x";
			} else {
				print " ";
			}
		}
		print "\n";
	}
}
